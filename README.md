# tui-mines

**tui mines** is evidently a text-based user interface implimentation of the classic  mine sweeping puzzle game.

The user has to clear a board, square by square, flagging the squares suspected to hide mines on the way.

If the user opens a mine square, things go **KABOOM!**  and the game is lost.

The user uses hints from the numbered squares. This numbers how many bombs are touching that square in every direction ( 8 in total). For instance a square with the number 2, will be 'touching' 2 bombs.

Through **logic**, and a bit of **luck**, the player ends up clearing all the squares, while flagging all the mines.

---

## Usage

![main menu](screenshots/main.png){width=300}


Opening the game, the user  through the main menu, can:

- Start a new game

![new game](screenshots/new1.png){width=300}

- Configure the parameters of the game according to taste

![edit preferences](screenshots/edit.png){width=300}

- Check out statistics

![top ten](screenshots/topten.png){width=300}

 - Just exit the game

![notification](screenshots/notification.png){height=80}

---
## Playing

The user can move the cursor with `h,j,k,l` (vim keys), or the arrow keys.

- As soon as the user hits the `reveal key` (`r` or `spacebar` by default), some squares will open.

![first move](screenshots/first-move.png){width=300}

- Slowly but surely the user goes on either revealing more squares (using the `r` key), or flagging suspicious squares, using the `f` key.

![flag1](screenshots/flag1.png){height=200}
![flag2](screenshots/flag2.png){height=200}
![flag3](screenshots/flag3.png){height=200}

- Open a square that contains a mine, and the game will invariantly end with a **Kaboom! ! !"


![kaboom](screenshots/kaboom.png){width=300}

- On the contrary, clearing all the empty squares and flagging all the hidden mines, using logic and sometimes having a little bit of luck, means that the user's mission is accomplished.

![win](screenshots/win.png){width=300}

---
### Using the Quick Reveal function

Once all the flags have been placed around a square, instead of revealing the surrounding squares one by one, the user can hit the **Quick Reveal key binding** (`e` by default). This way all the surrounding squares will be opened at once.



 ![quick reveal 1](screenshots/quick1.png){width=120}
 ![quick reveal 2](screenshots/quick2.png){width=120}

Needless to say that is if the flags are **wrongly placed**, and the user **Quickly Reveals** a mine, the game is **Quickly lost**.

---
## While Playing

While playing the user can

- Pause the game (`p,P`)

 ![pause](screenshots/pause.png){width=150}

- Toggle the appearence of the keybinding cheatsheet(`i,I`)

 ![cheatsheet](screenshots/cheatsheet.png){width=150}

- Solve the puzzle by quitting and returning to the main menu (`q,Q`)

 ![quit](screenshots/quit.png){width=150}




---
## Configuring

As mentioned above, the games parameters can be configured within the game from the `main menu`, or by editing the `$HOME/.config/tui-mines/tui-mines.config` file.

|n|Variable|Explanation| Acceptable Values|Default Value|
|---|---|---|---|---|
|1| WIDTH |Width of the grid|min 5, max 50(?)|10|
|2|HEIGHT|Height of the grid|min 5, max 50(?)|10|
|3|MINES_QUANTITY|Number of  mines in the grid||10|
|4|PREFERRED_EDITOR |Editor to be used to open the config file|Any gui or tui text editor|`$EDITOR`\|\|`nano`|
|5|NOTIFICATION_TOGGLE|Show Notifications while playing|yes / no| yes|
|6|CHEATSHEET_TOGGLE|Show keybinding cheatsheet by default (can be also toggled while playing)|yes / no| yes|
|7|COLORS_TOGGLE|Colors in the game|yes / no| yes|
|8|BLINK_TOGGLE|Blinking effect toggle|yes / no| yes|
|9|FLAG_BIND|Key binding for flagging squares|**CAUTION**: NON-ACCEPTABLE VALUES:upper-case A,B,C,D, lower AND upper-case H,J,K,L,I,P,Q|f|
|10|REVEAL_BIND|Key binding for revealing squares|**CAUTION**: NON-ACCEPTABLE VALUES:upper-case A,B,C,D, lower AND upper-case H,J,K,L,I,P,Q.**Also spacebar is hardcoded keybinding**|r|
|11|QUICK_SOLVE_BIND|Key binding for quick solving around squares with adequately flagged squares around them|**CAUTION**: NON-ACCEPTABLE VALUES:upper-case A,B,C,D, lower AND upper-case H,J,K,L,I,P,Q|e|
|12|MINE_CHAR|Character (simple or nerd-font) to represent the MINE|󰷚,🕱,☢,☠,,󰚑,󰚌,󰯆,󰯈,󰇷,󰱵,*,A-Z|󰷚|
|13|FLAG_CHAR|Character (simple or nerd-font) to represent the FLAGGED SQUARE.|󰈻,󰈽,,,󰈿,󰉀,󰈾,󰒡,󰲉,,󰜺,,,!,#,A-Z|󰈻|
|14|NAVIGATION_KEYS|Keys to navigate in the grid. Arrow keys are also hardcoded.| vim (hjkl), aswd |vim|

- Since nerd-fonts are not rendered properly in the repository page, here's a screenshot of the nerd-font characters in the config file:

![nerd-fonts](screenshots/nerd-fonts.png)

- If the configuration file is not properly loaded, the game will begin with hardcoded default values.

---

## Dependencies

The only dependency are [Nerd-fonts](https://www.nerdfonts.com/cheat-sheet), in order for the mine and flag characters to be rendered. However, is a user prefers minamal approaches, Nerd-fonts are not necessary, as long as normal characters are used instead (A-Z, !,# etc).

In order to install Nerd-fonts, download Ubuntu-Nerd-fonts (or some other font adequate for your system) from [https://www.nerdfonts.com/cheat-sheet](https://www.nerdfonts.com/cheat-sheet), and copy the exported `ttf`files to `~/.local/share/fonts/`.

Some systems may work differently, in order to install these fonts, the user may need to consult the respective manuals or fora.

---
## Install

- Clone to repository, and move to the `tui-mines` directory:

```
git clone https://gitlab.com/christosangel/tui-mines.git&&cd tui-mines/
```
- Make `install.sh` executable, then run it

```
chmod +x install.sh&&./install.sh
```
You are good to go. The script has been copied to `~/.local/bin/`, if this directory is in the system's `$PATH`, You can run it with

```
tui-mines.sh
```
otherwise, from the `tui-mines/` directory:

```
./tui-mines.sh
```

---

**Enjoy!**
