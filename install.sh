#! /bin/bash
#make sh executable, copy it to the $PATH
chmod +x tui-mines.sh
[[ -d $HOME/.local/bin/ ]]&&cp tui-mines.sh $HOME/.local/bin/&&INSTALL_MESSAGE="The script was copied to\n\e[33m $HOME/.local/bin/\e[m\nProvided that this directory is included in the '\$PATH', the user can run the script with\n\e[33m$ tui-mines.sh\e[m\nfrom any directory.\nAlternatively, the script can be run with\n\e[33m$ ./tui-mines.sh\e[m\nfrom the tui-mines/ directory."||INSTALL_MESSAGE="The script has been made executable and the user can run it with:\n\e[33m$ ./tui-mines.sh\e[m\nfrom the tui-mines/ directory."
# create the necessary directories and files:
mkdir -p $HOME/.local/share/tui-mines/ $HOME/.config/tui-mines/
cp  tui-mines.png $HOME/.local/share/tui-mines/


echo -e "#Width of the matrix
WIDTH=10
#Width of the matrix
HEIGHT=10

#Number of mines in the matrix
MINES_QUANTITY=10

#Text editor to open config file
PREFERRED_EDITOR=${EDITOR-nano}

#Acceptable notification togle values: yes / no
NOTIFICATION_TOGGLE=yes

#Acceptable notification toggle values: yes / no. This can also be controlled while playing
CHEATSHEET_TOGGLE=yes

#Color output. Acceptable notification toggle values: yes / no.
COLOR_TOGGLE=yes

#Blink effect toggle. Acceptable notification toggle values: yes / no.
BLINK_TOGGLE=yes

#Key binding to flag squares. Non acceptable values: Upper-case,q,i, arrow keys, navigation key-bindings.
FLAG_BIND=f

#Key binding to reveal squares. Spacebar is hardcoded, also. Non acceptable values: Upper-case,q,i, arrow keys, navigation key-bindings.
REVEAL_BIND=r

# Key binding for quick solving around squares with adequately flagged squares around them. Non acceptable values: Upper-case,q,i, arrow keys, navigation key-bindings.
QUICK_SOLVE_BIND=e

# Character (simple or nerd-font) to represent the MINE.
# Suggestions:󰷚,🕱,☢,☠,,󰚑,󰚌,󰯆,󰯈,󰇷,󰱵,*,A-Z
MINE_CHAR=󰷚

# Character (simple or nerd-font) to represent the FLAGGED SQUARE.
# Suggestions:󰈻,󰈽,,,󰈿,󰉀,󰈾,󰒡,󰲉,,󰜺,,,!,#,A-Z
FLAG_CHAR=󰈻

#Acceptable values for navigation keys: vim, aswd. Arrow keys are hardcoded and work in all options.
NAVIGATION_KEYS=vim">$HOME/.config/tui-mines/tui-mines.config
echo -e "$INSTALL_MESSAGE"
